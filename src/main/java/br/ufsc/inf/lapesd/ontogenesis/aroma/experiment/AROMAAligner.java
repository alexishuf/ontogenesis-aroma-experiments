package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import fr.inrialpes.exmo.align.impl.ObjectAlignment;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.aroma.AROMA;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Cell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static java.util.Arrays.asList;

public class AROMAAligner extends Aligner {
    private static Logger logger = LoggerFactory.getLogger(AROMAAligner.class);

    public static void main(String[] args) throws Exception {
        new AROMAAligner().parseAndRun(args);
    }

    @Override
    public Model align() throws AlignmentException, IOException {
        Model result = ModelFactory.createDefaultModel();
        Stopwatch watch = Stopwatch.createStarted();
        Model leftModel = load(left), rightModel = load(right);
        logger.info("Loaded input models with {} and {} triples in {}",
                leftModel.size(), rightModel.size(), watch);

        int properties = 0, classes = 0;
        try {
            AROMA aroma = setupAROMA(watch);
            Enumeration<Cell> elements = aroma.getElements();
            while (elements.hasMoreElements()) {
                Cell cell = elements.nextElement();
                if (!(cell.getRelation() instanceof EquivRelation)) continue;
                if (cell.getStrength() < threshold) continue;

                Resource rLeft = leftModel.createResource(cell.getObject1AsURI().toString());
                Resource rRight = rightModel.createResource(cell.getObject2AsURI().toString());

                boolean isClass = rLeft.hasProperty(RDF.type, OWL2.Class)
                        || rLeft.hasProperty(RDF.type, RDFS.Class)
                        || rRight.hasProperty(RDF.type, OWL2.Class)
                        || rRight.hasProperty(RDF.type, RDFS.Class);
                if (isClass) {
                    ++classes;
                    result.add(rLeft, OWL2.equivalentClass, rRight);
                } else {
                    ++properties;
                    result.add(rLeft, OWL2.equivalentProperty, rRight);
                }
            }
        } finally {
            Times times = new Times();
            times.alignMsecs = watch.elapsed(TimeUnit.MICROSECONDS)/1000.0;
            logger.info("AROMA found {} class and {} property alignments in {}",
                    classes, properties, watch);
            try (FileWriter writer = new FileWriter(timesFile)) {
                new Gson().toJson(times, writer);
            } catch (IOException e) {
                logger.error("Failed to write Times to {}.", timesFile, e);
            }
            leftModel.close();
            rightModel.close();
        }

        return result;
    }

    @Nonnull
    private AROMA setupAROMA(Stopwatch watch) throws AlignmentException {
        Properties properties = new Properties();
        AROMA aroma = new AROMA();
        aroma.skos = "true".equals(properties.getProperty("skos"));
        aroma.init(left.toURI(), right.toURI());
        watch.reset().start();
        aroma.align(new ObjectAlignment(), properties);
        return aroma;
    }
}
