package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.semanticweb.owl.align.AlignmentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;

@SuppressWarnings("WeakerAccess")
public abstract class Aligner {
    private static final Logger logger = LoggerFactory.getLogger(Aligner.class);

    @Option(name = "--help", aliases = {"-h"}, help = true)
    private boolean help = false;

    @Option(name = "--threshold", usage = "AROMA strength threshold")
    protected double threshold = 0.5;

    @Option(name = "--out", usage = "Output file of OWL alignments. Syntax will be guessed " +
            "from extension. If not defined, alignments will be outptu as Turtle to stdout")
    protected File out = null;
    @Option(name = "--times", usage = "Output a Times instance, in JSON to this file, " +
            "overwriting previous content")
    protected File timesFile = null;

    @Argument(index = 0)
    protected File left;
    @Argument(index = 1)
    protected File right;

    public void parseAndRun(String[] args) throws Exception {
        CmdLineParser parser = new CmdLineParser(this);
        parser.parseArgument(args);
        if (help) {
            parser.printUsage(System.out);
        } else {
            logger.info("Aligner implementation {}. Args: {}", getClass().getSimpleName(),
                    Arrays.stream(args).reduce((a, b) -> a + " " + b).orElse(""));
            run();
        }
    }

    public Aligner withThreshold(double threshold) {
        this.threshold = threshold;
        return this;
    }

    public Aligner withOut(File out) {
        this.out = out;
        return this;
    }

    public Aligner withTimesFile(File timesFile) {
        this.timesFile = timesFile;
        return this;
    }

    public Aligner withLeft(File left) {
        this.left = left;
        return this;
    }

    public Aligner withRight(File right) {
        this.right = right;
        return this;
    }

    public abstract Model align() throws Exception;

    protected static Model load(File file) throws IOException {
        Model model = ModelFactory.createDefaultModel();
        try (FileInputStream in = new FileInputStream(file)) {
            RDFDataMgr.read(model, in, RDFLanguages.filenameToLang(file.getName()));
        }
        return model;
    }

    protected OutputStream openOutputStream() throws IOException {
        if (out != null)
            return new FileOutputStream(out);
        return System.out;
    }

    protected Lang outputLang() {
        if (out != null)
            return RDFLanguages.filenameToLang(out.getName());
        else return Lang.TURTLE;
    }

    protected void run() throws Exception {
        try (OutputStream out = openOutputStream()) {
            RDFDataMgr.write(out, align(), outputLang());
        }
    }

    public ProcessBuilder childJVM(List<String> jvmOpts) {
        return ChildJVM.builder(getClass(), jvmOpts, createChildJVMArgs());
    }

    @Nonnull
    protected List<String> createChildJVMArgs() {
        List<String> args = new ArrayList<>();
        args.addAll(asList("--threshold", String.valueOf(threshold)));
        if (out != null)
            args.addAll(asList("--out", out.getPath()));
        if (timesFile != null)
            args.addAll(asList("--times", timesFile.getPath()));
        args.addAll(asList(left.getPath(), right.getPath()));
        return args;
    }

    public static class Times {
        double alignMsecs;
    }
}
