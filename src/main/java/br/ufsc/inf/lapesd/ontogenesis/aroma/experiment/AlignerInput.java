package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import br.ufsc.inf.lapesd.ld_jaxrs.core.traverser.CBDTraverser;
import br.ufsc.inf.lapesd.ld_jaxrs.jena.impl.model.JenaModelGraph;
import br.ufsc.inf.lapesd.ld_jaxrs.jena.impl.model.JenaNode;
import com.google.common.base.Stopwatch;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.compose.MultiUnion;
import org.apache.jena.iri.IRI;
import org.apache.jena.iri.IRIFactory;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.tdb.TDBFactory;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static java.lang.Math.min;

public class AlignerInput {
    private final static Map<String, String> prefixes;

    static {
        prefixes = new HashMap<>();
        prefixes.put("prov", "http://www.w3.org/ns/prov#");
        prefixes.put("dbo", "http://dbpedia.org/ontology/");
        prefixes.put("gn", "http://www.geonames.org/ontology#");
        prefixes.put("wgs84_post", "http://www.w3.org/2003/01/geo/wgs84_pos#");
        prefixes.put("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        prefixes.put("dcterms", "http://purl.org/dc/terms/");
        prefixes.put("dc", "http://purl.org/dc/elements/1.1/");
        prefixes.put("vs", "http://www.w3.org/2003/06/sw-vocab-status/ns#");
        prefixes.put("owl", "http://www.w3.org/2002/07/owl#");
        prefixes.put("dcam", "http://purl.org/dc/dcam/");
        prefixes.put("skos", "http://www.w3.org/2004/02/skos/core#");
        prefixes.put("wot", "http://xmlns.com/wot/0.1/");
        prefixes.put("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        prefixes.put("foaf", "http://xmlns.com/foaf/0.1/");
    }

    public static class Task implements Callable<File> {
        private static Logger logger = LoggerFactory.getLogger(Task.class);
        private static final long PROGRESS_INTERVAL = 10000;

        private final @Nonnull AlignerInputStrategy strategy;
        private final @Nonnull AlignerInputStrategy.Data d;
        private final @Nonnull File outputFile;
        private final boolean shouldPruneAromaSchema;
        private final boolean shouldPruneData;
        private long prunedDataTriples;
        private long badURITriples;
        private Stopwatch watch, lastProgressWatch;

        public Task(@Nonnull Model aromaSchema, @Nonnull Dataset dataset,
                    @Nonnull Set<String> langCodes, @Nonnull Model predefined,
                    boolean pruneAromaSchema, boolean pruneData,
                    @Nonnull AlignerInputStrategy strategy, @Nonnull File outputFile) {
            this.d = new AlignerInputStrategy.Data();
            this.d.schema = aromaSchema;
            this.d.dataset = dataset;
            this.d.langCodes = langCodes;
            this.d.predefined = predefined;
            this.d.output = ModelFactory.createDefaultModel();
            this.d.output.setNsPrefixes(d.dataset.getDefaultModel().getNsPrefixMap());
            this.shouldPruneAromaSchema = pruneAromaSchema;
            this.shouldPruneData = pruneData;
            this.strategy = strategy;
            this.outputFile = outputFile;
        }

        @Override
        public File call() throws Exception {
            try {
                return run();
            } finally {
                strategy.close();
                d.schema.close();
                d.dataset.close();
            }
        }

        private File run() throws Exception {
            this.watch = Stopwatch.createStarted();

            strategy.init(d);
            lastProgressWatch = Stopwatch.createStarted();
            while (strategy.hasNextStep()) {
                CBDTraverser cbdTraverser = new CBDTraverser().setReifications(false);

                strategy.nextStep().forEach(resource -> {
                    Model temp = ModelFactory.createDefaultModel();
                    cbdTraverser.traverse(new JenaModelGraph(resource.getModel()),
                            new JenaNode(resource), new JenaModelGraph(temp));
                    filterAndAdd(d.output, temp);
                    updateProgress(false);
                });
            }
            updateProgress(true);

            postProcessSchema();
            Lang lang = RDFLanguages.filenameToLang(outputFile.getName());
            if (lang == null)
                lang = Lang.RDFXML;
            try (FileOutputStream out = new FileOutputStream(outputFile)) {
                Model union = ModelFactory.createModelForGraph(new MultiUnion(new Graph[]
                        {d.output.getGraph(), d.schema.getGraph(), d.predefined.getGraph()}));
                fillInPrefixes(union);

                RDFDataMgr.write(out, union, lang);
                out.flush();
                logger.info("Alignment file output with {} triples ({} new), {} MiB\n",
                        union.size(), union.size()-d.predefined.size(),
                        outputFile.length()/1048576.0);
            }
            d.output.close();
            return outputFile;
        }

        private void fillInPrefixes(Model model) {
            for (Map.Entry<String, String> e : prefixes.entrySet()) {
                String uri = e.getValue();
                if (model.getNsURIPrefix(uri) == null) {
                    String prefix = e.getKey();
                    while (model.getNsPrefixURI(prefix) != null)
                        prefix += "X";
                    model.setNsPrefix(prefix, uri);
                }
            }
        }

        private synchronized void updateProgress(boolean force) {
            if (!force && lastProgressWatch.elapsed(TimeUnit.MILLISECONDS) < PROGRESS_INTERVAL)
                return;
            lastProgressWatch.reset().start();
            logger.info("Processed {}%. {} instance triples output, {} pruned, {} removed due to " +
                            "bad IRIs. {} elapsed.", Math.round(strategy.getProgress()*100),
                    d.output.size(), prunedDataTriples, badURITriples, watch);
        }

        private void postProcessSchema() {
            if (shouldPruneAromaSchema) pruneSchema(d.schema);

            removeExtraRDFProperty();
            removeUndefinedProperties();
        }

        private void removeUndefinedProperties() {
            Set<Resource> defined = new HashSet<>();
            Stream.of(OWL2.ObjectProperty, OWL2.DatatypeProperty, RDF.Property).forEach(type ->
                    defined.addAll(d.schema.listResourcesWithProperty(RDF.type, type).toList()));

            List<Statement> victims = new ArrayList<>();
            Stream.of(OWL2.equivalentProperty, RDFS.subPropertyOf, OWL2.onProperty).forEach(p -> {
                StmtIterator it = d.schema.listStatements(null, p, (RDFNode) null);
                while (it.hasNext()) victims.add(it.next());
                it.close();
            });
            d.schema.remove(victims);
            logger.info("Removed {} references to undefined properties", victims.size());
        }

        private void removeExtraRDFProperty() {
            StmtIterator it = d.schema.listStatements(null, RDF.type, RDF.Property);
            List<Statement> victims = new ArrayList<>();
            while (it.hasNext()) {
                Statement s = it.next();
                boolean bad = s.getSubject().hasProperty(RDF.type, OWL2.ObjectProperty)
                        || s.getSubject().hasProperty(RDF.type, OWL2.DatatypeProperty);
                if (bad) victims.add(s);
            }
            d.schema.remove(victims);
            logger.info("Removed {} rdf:Property assertions to make AROMA happy", victims.size());
        }

        private void pruneSchema(Model m) {
            Model data = d.dataset.getDefaultModel();

            Set<Resource> properties = new HashSet<>(), classes = new HashSet<>();
            properties.addAll(m.listSubjectsWithProperty(RDF.type, OWL2.DatatypeProperty).toList());
            properties.addAll(m.listSubjectsWithProperty(RDF.type, OWL2.ObjectProperty).toList());
            properties.addAll(m.listSubjectsWithProperty(RDF.type, RDF.Property).toList());
            classes.addAll(m.listSubjectsWithProperty(RDF.type, OWL2.Class).toList());
            classes.addAll(m.listSubjectsWithProperty(RDF.type, RDFS.Class).toList());

            properties.removeIf(p -> !data.listStatements(null, p.as(Property.class),
                    (RDFNode)null).hasNext());
            classes.removeIf(c -> !data.listStatements(null, RDF.type, c).hasNext());
            classes.add(m.createResource(OWL2.Ontology.getURI())); //do not remove!

            Model pruned = ModelFactory.createDefaultModel();
            pruned.setNsPrefixes(d.schema.getNsPrefixMap());
            CBDTraverser traverser = new CBDTraverser()
                    .setTraverseNamed(true)
                    .setMaxPathFromFirstNamed(1)
                    .setSymmetric(true);
            JenaModelGraph mGraph = new JenaModelGraph(m);
            JenaModelGraph prunedGraph = new JenaModelGraph(pruned);
            Stream.concat(properties.stream(), classes.stream()).map(JenaNode::new)
                    .forEach(node -> traverser.traverse(mGraph, node, prunedGraph));
            logger.info("Pruned {} triples from schema", d.schema.size()-pruned.size());

            d.schema.removeAll();
            d.schema.add(pruned);
            pruned.close();
        }

        private synchronized void filterAndAdd(Model output, Model temp) {
            long added = 0;
            long withBadURIs = 0;
            for (StmtIterator it = temp.listStatements(); it.hasNext(); ) {
                Statement s = it.next();
                if (s.getPredicate().isAnon()) continue;
                if (isBadURI(s.getSubject()) || isBadURI(s.getObject())) {
                    withBadURIs++;
                    continue;
                }
                if (shouldPruneData) {
                    String uri = s.getPredicate().getURI();
                    if (d.schema.createResource(uri).listProperties().hasNext()) {
                        ++added;
                        output.add(s);
                    }
                } else {
                    output.add(s);
                }
            }
            prunedDataTriples += temp.size() - (added - withBadURIs);
            badURITriples += withBadURIs;
        }

        private boolean isBadURI(RDFNode node) {
            if (!node.isURIResource()) return false;
            IRI iri = IRIFactory.iriImplementation().create(node.asResource().getURI());
            return iri.hasViolation(false);
        }
    }

    public static class Main {
        @Option(name = "--help", aliases = {"-h"}, help = true)
        private boolean help;

        @Option(name = "--index", usage = "DBMap database file with the " +
                "ontogenesis index for external resources already constructed.")
        private File indexFile;

        @Option(name = "--strategy-frequency", depends = {"--index"},
                forbids = {"--strategy-max-triples", "--strategy-proportional"},
                usage = "Value in (0, 1] representing the percentage of most frequent object " +
                        "values to be considered in rebuilding the data fraction.")
        private double frequencyThreshold = -1;

        @Option(name = "--strategy-max-triples",
                forbids = {"--strategy-frequency", "--strategy-proportional"},
                usage = "randomly select CBDs of instances, until the whole instance triples " +
                        "exceed this number")
        private int maxTriples = -1;

        @Option(name = "--strategy-proportional",
                forbids = {"--proportional-triples", "--strategy-frequency"},
                usage = "Same as --strategy-max-triples X, where X is the number of " +
                        "property-object pairs that would be considered by ontogenesis under " +
                        "the specified --proportional-frequency. The value given is a number in " +
                        "[0, 1] representing the percentage of the ontogenesis indexes used to " +
                        "calculate the number of triples")
        private double proportionalTriples = -1;

        @Option(name = "--strategy-full",
                forbids = {"--strategy-frequency", "--strategy-max-triples",
                        "--strategy-proportional"},
                usage = "Use a dump of the data source as the instance data. Will not perform a " +
                        "filtering of the subset beyond pruning and discarding bad URIs.")
        private boolean strategyFull = false;

        @Option(name = "--schema", required = true,
                usage = "Ontology (schema-only) of data that will be given to AROMA")
        private File[] schemas;

        @Option(name = "--prune-schema", usage = "Instead of outputting the merge of all " +
                "schemas in --schemas, only the CBD of the properties used by data will be " +
                "output to AROMA.")
        private boolean pruneSchema = false;

        @Option(name = "--prune-data", usage = "Causes instance data to contain only properties " +
                "which are defined in the merge of all --schemas. The presence of a triple " +
                "(?s <predicate> ?o) counts as <predicate> being defined.")
        private boolean pruneData = false;

        @Option(name = "--append", usage = "Appends instance and schema data to the output file. " +
                "Pruning (of ontology or instances) will not be applied to data already present " +
                "at the output, but will be applied to newly added data.")
        private boolean append = false;

        @Option(name = "--lang", usage = "Set of language tags to try when querying the data")
        private String[] langs = {"en"};

        @Option(name = "--data-tdb",
                usage = "Local TDB directory with data for querying")
        private File dataTDB;

        @Option(name = "--out", required = true)
        public File output;

        public static void main(String[] args) throws Exception {
            Main main = new Main();
            CmdLineParser parser = new CmdLineParser(main);
            parser.parseArgument(args);
            if (main.frequencyThreshold > 1)
                throw new IllegalArgumentException("--strategy-frequency must be in [0, 1]");
            if (main.proportionalTriples > 1)
                throw new IllegalArgumentException("--proportional-frequency must be in [0, 1]");
            if (main.help)
                parser.printUsage(System.out);
            else
                main.run();
        }

        private void run() throws Exception {
            Model aromaOntology = ModelFactory.createDefaultModel();
            Model predefined = ModelFactory.createDefaultModel();
            Dataset dataset;
            try {
                AlignerInputStrategy strategy = loadStrategy();

                if (append)
                    load(predefined, output);

                loadEnsuringSingleOntology(aromaOntology, schemas, predefined);

                dataset = TDBFactory.createDataset(dataTDB.getPath());
                if (dataset.getDefaultModel().size() == 0) {
                    throw new IllegalArgumentException(String.format(
                            "TDB dataset at %s is empty", dataTDB.getPath()));
                }

                new Task(aromaOntology, dataset, new HashSet<>(Arrays.asList(langs)),
                        predefined, pruneSchema, pruneData, strategy, output).call();
            } finally {
                aromaOntology.close();
            }
        }

        private AlignerInputStrategy loadStrategy() {
            if (frequencyThreshold > 0) {
                DB db = DBMaker.fileDB(indexFile).fileMmapEnable().make();
                return new MostFrequentStrategy(db, frequencyThreshold);
            } else if (maxTriples > 0) {
                return new MaxTriplesStrategy(maxTriples);
            } else if (proportionalTriples > 0) {
                DB db = DBMaker.fileDB(indexFile).fileMmapEnable().make();
                return new ProportionalTriplesStrategy(db, proportionalTriples);
            } else if (strategyFull) {
                return new FullStrategy();
            }
            throw new IllegalArgumentException("No --strategy-* specified!");
        }


        private void loadEnsuringSingleOntology(Model output, File[] files,
                                                Model predefined) throws IOException {
            List<Resource> ontologies = predefined
                    .listSubjectsWithProperty(RDF.type, OWL2.Ontology).toList();
            if (ontologies.size() > 1) {
                throw new IllegalArgumentException("--append is set and output file already has " +
                        "more than one owl:Ontology instance.");
            }
            Resource singleOntology = ontologies.isEmpty() ? null : ontologies.get(0);

            Model model = ModelFactory.createDefaultModel();
            for (File file : files) {
                load(model, file);
                ontologies = model.listSubjectsWithProperty(RDF.type, OWL2.Ontology).toList();

                if (singleOntology == null && !ontologies.isEmpty()) {
                    if (ontologies.size() > 1) {
                        throw new IllegalArgumentException("File " + file
                                + "has multiple owl:Ontology instances.");
                    }
                    Resource o = ontologies.get(0);
                    singleOntology = o.isAnon() ? output.createResource(o.getId())
                            : output.createResource(o.getURI());
                } else {
                    for (Resource o : ontologies) {
                        model.removeAll(o, null, null);
                        model.removeAll(null, null, o);
                    }
                }

                output.add(model);
                model.removeAll();
            }
        }

        private void load(Model out, File file) throws IOException {
            try (FileInputStream in = new FileInputStream(file)) {
                Lang lang = RDFLanguages.filenameToLang(file.getName());
                RDFDataMgr.read(out, in, lang);
            }
        }
    }

    public static ProcessBuilder childJVM(List<String> jvmOpts, List<String> args) {
        return ChildJVM.builder(Main.class, jvmOpts, args);
    }
}
