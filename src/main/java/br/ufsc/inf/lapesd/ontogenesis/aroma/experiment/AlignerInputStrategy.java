package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import javax.annotation.Nonnull;
import java.util.Set;
import java.util.stream.Stream;

public interface AlignerInputStrategy extends AutoCloseable {
    @SuppressWarnings("WeakerAccess")
    class Data {
        public Model schema;
        public Dataset dataset;
        public Set<String> langCodes;
        public Model predefined;
        public Model output;
    }

    void init(@Nonnull Data data);
    @Nonnull Stream<Resource> nextStep();
    boolean hasNextStep();
    double getProgress();
}
