package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChildJVM {
    public static ProcessBuilder builder(Class<?> clazz, List<String> jvmOpts,
                                         List<String> mainArgs) {
        String separator = System.getProperty("file.separator");
        String classpath = System.getProperty("java.class.path");
        String java = System.getProperty("java.home")
                + separator + "bin" + separator + "java";

        List<String> args = new ArrayList<>(Arrays.asList(java, "-cp", classpath));
        args.addAll(jvmOpts);
        args.add(clazz.getName());
        args.addAll(mainArgs);

        return new ProcessBuilder(args);
    }
}
