package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.TeeInputStream;
import org.apache.commons.io.output.CloseShieldOutputStream;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static java.lang.String.valueOf;
import static java.util.Arrays.asList;

public class Experiment {
    private final static Logger logger = LoggerFactory.getLogger(Experiment.class);

    enum AlignerType {
        AROMA,
        PARIS;

        Aligner createAligner() {
            switch (this) {
                case AROMA: return new AROMAAligner();
                case PARIS: return new PARISAligner();
            }
            throw new UnsupportedOperationException();
        }
    }

    public enum CSVHeader {
        NUMBER,
        NAME,
        ALIGNER,
        CRIMINAL_REPORT_REPOSITORY,
        DBPEDIA_LOAD_ARGS,
        GEONAMES_LOAD_ARGS,
        PERSON_COUNT_FROM,
        PERSON_COUNT_TO,
        THRESHOLD,
        JVM_OPTS
    }

    public static class Result {
        public enum CSVHeader {
            NUMBER,
            PERSON_COUNT,
            AROMA_MSECS,
            EXIT_VALUE
        }

        public final int number, personCount, exitValue;
        public final double aromaMsecs;

        public Result(int number, int personCount, double aromaMsecs, int exitValue) {
            this.number = number;
            this.personCount = personCount;
            this.aromaMsecs = aromaMsecs;
            this.exitValue = exitValue;
        }

        public Result(CSVRecord record) {
            this(Integer.parseInt(record.get(CSVHeader.NUMBER)),
                    Integer.parseInt(record.get(CSVHeader.PERSON_COUNT)),
                    Double.parseDouble(record.get(CSVHeader.AROMA_MSECS)),
                    Integer.parseInt(record.get(CSVHeader.EXIT_VALUE)));
        }

        public List<String> toStrings() {
            return asList(valueOf(number), valueOf(personCount), valueOf(aromaMsecs),
                    valueOf(exitValue));
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Result result = (Result) o;

            if (number != result.number) return false;
            if (exitValue != result.exitValue) return false;
            return Double.compare(result.aromaMsecs, aromaMsecs) == 0;
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            result = number;
            result = 31 * result + exitValue;
            temp = Double.doubleToLongBits(aromaMsecs);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }

        @Override
        public String toString() {
            return String.format("Experiment.Result(%s)",
                    toStrings().stream().reduce((a, b) -> a + ", " + b).orElse(""));
        }
    }


    private int number;
    private @Nonnull String description;
    private @Nonnull AlignerType alignerType;
    private @Nonnull File criminalReportRepository;
    private @Nonnull List<String> dbpediaLoadArgs, geonamesLoadArgs;
    private int personCountFrom, personCountTo;
    private double threshold;
    private @Nonnull List<String> jvmOpts;

    private static List<String> tsvList(String tsv) {
        return Arrays.stream(tsv.split("\t")).map(String::trim).filter(s -> !s.isEmpty())
                .map(s -> s.replace("\\;", ","))
                .collect(Collectors.toList());
    }

    public Experiment(int number, @Nonnull String description,
                      @Nonnull AlignerType alignerType, @Nonnull File criminalReportRepository, @Nonnull List<String> dbpediaLoadArgs,
                      @Nonnull List<String> geonamesLoadArgs, int personCountFrom, int personCountTo,
                      double threshold, @Nonnull List<String> jvmOpts) {
        this.number = number;
        this.description = description;
        this.alignerType = alignerType;
        this.criminalReportRepository = criminalReportRepository;
        this.dbpediaLoadArgs = dbpediaLoadArgs;
        this.geonamesLoadArgs = geonamesLoadArgs;
        this.personCountFrom = personCountFrom;
        this.personCountTo = personCountTo;
        this.threshold = threshold;
        this.jvmOpts = jvmOpts;
    }

    public Experiment(CSVRecord record) {
        this(Integer.parseInt(record.get(CSVHeader.NUMBER)), record.get(CSVHeader.NAME),
                AlignerType.valueOf(record.get(CSVHeader.ALIGNER)),
                new File(record.get(CSVHeader.CRIMINAL_REPORT_REPOSITORY)),
                tsvList(record.get(CSVHeader.DBPEDIA_LOAD_ARGS)),
                tsvList(record.get(CSVHeader.GEONAMES_LOAD_ARGS)),
                Integer.parseInt(record.get(CSVHeader.PERSON_COUNT_FROM)),
                Integer.parseInt(record.get(CSVHeader.PERSON_COUNT_TO)),
                Double.parseDouble(record.get(CSVHeader.THRESHOLD)),
                tsvList(record.get(CSVHeader.JVM_OPTS)));
    }

    public void run(File dir, Consumer<Result> consumer) throws Exception {
        logger.info("Running experiment {}", this);

        File persons = new File(new File(dir, "inputs"), "persons-" + number + ".owl");
        PoliceReportPerson2RDF.Task personsTask = setupPersons(persons);

        File external = new File(new File(dir, "inputs"), "external-" + number + ".owl");
        if (!external.exists())
            setupExternal(external);

        for (int personCount = personCountFrom; personCount <= personCountTo; personCount++) {
            personsTask.addPersonUntil(personCount);

            File outputs = new File(dir, "outputs");
            File out = new File(outputs, "alignments-" + number + "-" + personCount + ".owl");
            File log = new File(outputs, "jvm-" + number + "-" + personCount + ".log");
            File timesFile = Files.createTempFile("", ".json").toFile();
            timesFile.deleteOnExit();

            Aligner aligner = alignerType.createAligner().withLeft(persons).withRight(external)
                    .withOut(out).withThreshold(threshold).withTimesFile(timesFile);

            Process p = aligner.childJVM(jvmOpts).redirectErrorStream(true)
                    .redirectOutput(ProcessBuilder.Redirect.PIPE).start();
            try (FileOutputStream logOut = new FileOutputStream(log);
                 TeeOutputStream teeOut = new TeeOutputStream(logOut, new CloseShieldOutputStream(System.out));
                 TeeInputStream teeIn = new TeeInputStream(p.getInputStream(), teeOut)) {
                IOUtils.skip(teeIn, Long.MAX_VALUE);
                p.waitFor();
            }
            int exitValue = p.exitValue();
            double  aromaMsecs = -1;
            try (FileReader reader = new FileReader(timesFile)) {
                Aligner.Times times = new Gson().fromJson(reader, Aligner.Times.class);
                aromaMsecs = times.alignMsecs;
            } catch (IOException e) {
                logger.error("Failed to read json from {}", timesFile, e);
            } finally {
                if (!timesFile.delete())
                    logger.error("Failed to delete timesFile={}", timesFile);
            }

            consumer.accept(new Result(number, personCount, aromaMsecs, exitValue));
        }
    }

    private void setupExternal(File external) throws Exception {
        Stopwatch watch = Stopwatch.createStarted();

        ArrayList<String> args = new ArrayList<>(dbpediaLoadArgs);
        args.addAll(asList("--out", external.getPath()));
        logger.info("Generating dbpedia data. args: {}. jvmOpts: {}", args, jvmOpts);
        Process process = AlignerInput.childJVM(jvmOpts, args).redirectErrorStream(true)
                .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                .start();
        process.waitFor();
        if (process.exitValue() != 0)
            throw new Exception("AlignerInput child jvm exited with status " + process.exitValue());


        args = new ArrayList<>(geonamesLoadArgs);
        args.addAll(asList("--append", "--out", external.getPath()));
        logger.info("Appending geonames data. args: {}. jvmOpts: {}", args, jvmOpts);
        process = AlignerInput.childJVM(jvmOpts, args).redirectErrorStream(true)
                .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                .start();
        process.waitFor();
        if (process.exitValue() != 0)
            throw new Exception("AlignerInput child jvm exited with status " + process.exitValue());

        logger.info("Setup of external ontology+instance file took {}", watch);
    }

    private PoliceReportPerson2RDF.Task setupPersons(File persons) throws IOException {
        if (persons.exists() && !persons.delete())
            throw new IOException("Could not delete " + persons);
        PoliceReportPerson2RDF.Repository repository;
        repository = new PoliceReportPerson2RDF.Repository(criminalReportRepository);
        Model personOntology = ModelFactory.createDefaultModel();
        ClassLoader cl = getClass().getClassLoader();
        try (InputStream in = cl.getResourceAsStream("criminal-reports-ontology.owl")) {
            RDFDataMgr.read(personOntology, in, Lang.RDFXML);
        }
        return new PoliceReportPerson2RDF.Task(repository.iterator(), persons,
                Lang.RDFXML, personOntology);
    }

    private static String joinTsv(@Nonnull List<String> list) {
        return list.stream()
                .map(s -> s.replace(",", "\\;"))
                .reduce((a, b) -> a + "\t" + b).orElse("");
    }

    public List<String> toStrings() {
        return asList(valueOf(number), description, alignerType.name(),
                criminalReportRepository.getPath(), joinTsv(dbpediaLoadArgs),
                joinTsv(geonamesLoadArgs), valueOf(personCountFrom), valueOf(personCountTo),
                valueOf(threshold), joinTsv(jvmOpts));
    }

    @Override
    public String toString() {
        return String.format("Experiment(%s)",
                toStrings().stream().reduce((a, b) -> a + ", " + b).orElse(""));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Experiment that = (Experiment) o;

        if (number != that.number) return false;
        if (personCountFrom != that.personCountFrom) return false;
        if (personCountTo != that.personCountTo) return false;
        if (Double.compare(that.threshold, threshold) != 0) return false;
        if (!description.equals(that.description)) return false;
        if (alignerType != that.alignerType) return false;
        if (!criminalReportRepository.equals(that.criminalReportRepository)) return false;
        if (!dbpediaLoadArgs.equals(that.dbpediaLoadArgs)) return false;
        if (!geonamesLoadArgs.equals(that.geonamesLoadArgs)) return false;
        return jvmOpts.equals(that.jvmOpts);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = number;
        result = 31 * result + description.hashCode();
        result = 31 * result + alignerType.hashCode();
        result = 31 * result + criminalReportRepository.hashCode();
        result = 31 * result + dbpediaLoadArgs.hashCode();
        result = 31 * result + geonamesLoadArgs.hashCode();
        result = 31 * result + personCountFrom;
        result = 31 * result + personCountTo;
        temp = Double.doubleToLongBits(threshold);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + jvmOpts.hashCode();
        return result;
    }
}
