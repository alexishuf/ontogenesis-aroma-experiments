package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.io.Files;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ExperimentRunner {
    private static final Logger logger = LoggerFactory.getLogger(ExperimentRunner.class);

    @Option(name = "--help", aliases = {"-h"}, help = true)
    private boolean help;

    @Option(name = "--run-dir",  usage = "Directory where the experiments will run")
    private File runDir = new File("run");

    @Argument(index = 0, required = true)
    private File experimentsFile;

    @Argument(index = 1, required = true)
    private File resultsFile;

    public static void main(String[] args) throws Exception {
        ExperimentRunner runner = new ExperimentRunner();
        CmdLineParser parser = new CmdLineParser(runner);
        parser.parseArgument(args);
        if (runner.help)
            parser.printUsage(System.out);
        else
            runner.run();
    }

    private void run() throws Exception {
        if (runDir.exists()) {
            logger.info("Deleting old run dir {}", runDir);
            if (runDir.isDirectory()) {
                FileUtils.deleteDirectory(runDir);
            } else if (!runDir.delete()) {
                throw new IOException("runDir " + runDir + " is a regular file and could't be deleted");
            }
        }
        if (!runDir.mkdirs())
            throw new IOException("Could not mkdir " + runDir);
        if (!new File(runDir, "inputs").mkdir())
            throw new IOException("Could not mkdir inputs");
        if (!new File(runDir, "outputs").mkdir())
            throw new IOException("Could not mkdir outputs");

        List<Experiment> experiments = new CSVParser(new FileReader(experimentsFile),
                    CSVFormat.DEFAULT.withHeader().withIgnoreSurroundingSpaces())
                .getRecords().stream().map(Experiment::new)
                .collect(Collectors.toList());

        try (FileWriter fileWriter = new FileWriter(resultsFile);
             CSVPrinter csvPrinter = new CSVPrinter(fileWriter, CSVFormat.DEFAULT)) {
            for (int i = 0; i < experiments.size(); i++) {
                Experiment experiment = experiments.get(i);
                Stopwatch watch = Stopwatch.createStarted();
                experiment.run(runDir, r -> {
                    try {
                        csvPrinter.printRecord(r.toStrings());
                        csvPrinter.flush();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
                logger.info("Ran experiment {}/{} in {}", i, experiments.size(), watch);
            }
        }
    }
}
