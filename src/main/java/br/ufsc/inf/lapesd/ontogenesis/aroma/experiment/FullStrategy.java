package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

public class FullStrategy implements AlignerInputStrategy {
    private final static Logger logger = LoggerFactory.getLogger(FullStrategy.class);
    private static final int PROGRESS_INTERVAL = 5000;

    private Data data;
    private Iterator<Resource> subjectsIt;
    private Set<Resource> subjects;
    private long consumed = 0;

    @Override
    public void init(@Nonnull Data data) {
        Preconditions.checkState(this.data == null);
        this.data = data;
        Model m = data.dataset.getDefaultModel();
        this.subjects = new HashSet<>();
        Stopwatch startWatch = Stopwatch.createStarted();
        Stopwatch progressWatch = Stopwatch.createStarted();
        long total = m.size(), done = -1;
        for (StmtIterator it = m.listStatements(); it.hasNext(); ) {
            ++done;
            if (progressWatch.elapsed(TimeUnit.MILLISECONDS) >= PROGRESS_INTERVAL) {
                progressWatch.reset().start();
                logger.info("Computing subjects {}/{} ({}%) elapsed: {}", done, total,
                        Math.round(done/(double)total*100), startWatch);
            }
            subjects.add(it.next().getSubject());
        }
        logger.info("Found {} subjects in {}", subjects.size(), startWatch);

        subjectsIt = subjects.iterator();
        consumed = 0;
    }

    @Nonnull
    @Override
    public Stream<Resource> nextStep() {
        ++consumed;
        return Stream.of(subjectsIt.next());
    }

    @Override
    public boolean hasNextStep() {
        return subjectsIt.hasNext();
    }

    @Override
    public double getProgress() {
        return consumed/(double)subjects.size();
    }

    @Override
    public void close() throws Exception {
        subjects = null;
        subjectsIt = null;
    }
}
