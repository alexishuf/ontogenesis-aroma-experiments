package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import com.google.common.base.Stopwatch;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.tdb.TDBFactory;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static org.apache.jena.rdf.model.ResourceFactory.*;

public class Geonames2RDF {
    private static Logger logger = LoggerFactory.getLogger(Geonames2RDF.class);
    private static int PROGRESS_INTERVAL = 5000;
    private static final String[] CSV_HEADERS = {
            "geonameid",        // 0
            "name",             // 1
            "asciiname",        // 2
            "alternatenames",   // 3
            "latitude",         // 4
            "longitude",        // 5
            "feature class",    // 6
            "feature code",     // 7
            "country code",     // 8
            "cc2",              // 9
            "admin1 code",      //10
            "admin2 code",      //11
            "admin3 code",      //12
            "admin4 code",      //13
            "population",       //14
            "elevation",        //15
            "dem",              //16
            "timezone",         //17
            "modification date" //18
    };
    private static final CSVFormat CSV_FORMAT = CSVFormat.TDF.withHeader(CSV_HEADERS);
    private static final Charset CSV_CHARSET = StandardCharsets.UTF_8;
    private static final String GN_NS = "http://www.geonames.org/ontology#";
    private static final String WGS84_NS = "http://www.w3.org/2003/01/geo/wgs84_pos#";

    private static final Property countryCode    = createProperty(GN_NS+"countryCode");
    private static final Property population     = createProperty(GN_NS+"population");
    private static final Property shortName      = createProperty(GN_NS+"shortName");
    private static final Property name           = createProperty(GN_NS+"name");
    private static final Property historicalName = createProperty(GN_NS+"historicalName");
    private static final Property officialName   =  createProperty(GN_NS+"officialName");
    private static final Property colloquialName = createProperty(GN_NS+"colloquialName");
    private static final Property postalCode     = createProperty(GN_NS+"postalCode");
    private static final Property geonamesID     = createProperty(GN_NS+"geonamesID");
    private static final Property featureClass     = createProperty(GN_NS+"featureClass");
    private static final Property featureCode     = createProperty(GN_NS+"featureCode");
    private static final Property parentADM1     = createProperty(GN_NS+"parentADM1");
    private static final Property parentADM2     = createProperty(GN_NS+"parentADM2");
    private static final Property parentADM3     = createProperty(GN_NS+"parentADM3");
    private static final Property parentADM4     = createProperty(GN_NS+"parentADM4");

    private static final Property wgs84_lat  = createProperty(WGS84_NS+"lat");
    private static final Property wgs84_long = createProperty(WGS84_NS+"long");


    private static final Map<String, Property> csv2Property;
    private static final Map<String, Function<String, List<RDFNode>>> csvParsers;
    private static final String GN_SWS = "http://sws.geonames.org/";

    static {
        csv2Property = new HashMap<>();
        csv2Property.put(CSV_HEADERS[ 0], geonamesID);   //geonameid
        csv2Property.put(CSV_HEADERS[ 1], name);         //name
        csv2Property.put(CSV_HEADERS[ 3], name);         //alternatenames
        csv2Property.put(CSV_HEADERS[ 4], wgs84_lat);    //latitude
        csv2Property.put(CSV_HEADERS[ 5], wgs84_long);   //longitude
        csv2Property.put(CSV_HEADERS[ 6], featureClass); //feature class
        csv2Property.put(CSV_HEADERS[ 7], featureCode);  //feature code
        csv2Property.put(CSV_HEADERS[ 8], countryCode);  //country code
//        csv2Property.put(CSV_HEADERS[10], parentADM1); //admin1 code
//        csv2Property.put(CSV_HEADERS[11], parentADM2); //admin2 code
//        csv2Property.put(CSV_HEADERS[12], parentADM3); //admin3 code
//        csv2Property.put(CSV_HEADERS[13], parentADM4); //admin4 code
        csv2Property.put(CSV_HEADERS[14], population);   //population

        csvParsers = new HashMap<>();
        Function<String, List<RDFNode>> plain, split, admin, featureClass;
        plain = s -> singletonList(createPlainLiteral(s));
        split = s -> Arrays.stream(s.split(",")).map(ResourceFactory::createPlainLiteral)
                .collect(Collectors.toList());
        admin = s -> singletonList(createResource("http://sws.geonames.org/"+s));
        featureClass = s -> singletonList(createResource(GN_NS+s));

        csvParsers.put(CSV_HEADERS[ 0], plain);        //geonameid
        csvParsers.put(CSV_HEADERS[ 1], plain);        //name
        csvParsers.put(CSV_HEADERS[ 3], split);        //alternatenames
        csvParsers.put(CSV_HEADERS[ 4], plain);        //latitude
        csvParsers.put(CSV_HEADERS[ 5], plain);        //longitude
        csvParsers.put(CSV_HEADERS[ 6], featureClass); //feature class
//        csvParsers.put(CSV_HEADERS[ 7], );           //feature code
        csvParsers.put(CSV_HEADERS[ 8], plain);        //country code
//        csvParsers.put(CSV_HEADERS[10], admin);      //admin1 code
//        csvParsers.put(CSV_HEADERS[11], admin);      //admin2 code
//        csvParsers.put(CSV_HEADERS[12], admin);      //admin3 code
//        csvParsers.put(CSV_HEADERS[13], admin);      //admin4 code
        csvParsers.put(CSV_HEADERS[14], plain);        //population
    }


    public static class Task implements Callable<Model> {
        private final @Nonnull CSVParser parser;
        private final @Nonnull Model output;
        private boolean onlyName;

        public Task(@Nonnull CSVParser parser, @Nonnull Model output) {
            this.parser = parser;
            this.output = output;
        }

        @Override
        public Model call() throws Exception {
            final String progressTpl = "Processed {} records into {} triples, {} elapsed.";

            try {
                Stopwatch startWatch = Stopwatch.createStarted();
                Stopwatch progressWatch = Stopwatch.createStarted();
                for (CSVRecord r : parser) {
                    convert(output, r);
                    if (progressWatch.elapsed(TimeUnit.MILLISECONDS) > PROGRESS_INTERVAL) {
                        logger.info(progressTpl, r.getRecordNumber(), output.size(), startWatch);
                        progressWatch.reset().start();
                    }
                }
                logger.info(progressTpl, parser.getCurrentLineNumber(), output.size(), startWatch);
                logger.info("Conversion complete");
                return output;
            } finally {
                parser.close();
            }
        }

        private void convert(Model output, CSVRecord record) {
            Resource resource = output.createResource(GN_SWS + record.get(CSV_HEADERS[0]));
            for (String hdr : CSV_HEADERS) {
                String value = record.get(hdr);
                if (value == null || value.isEmpty()) continue;
                if (onlyName && !hdr.equals(CSV_HEADERS[1])) continue;

                Function<String, List<RDFNode>> parser = csvParsers.getOrDefault(hdr, null);
                Property property = csv2Property.get(hdr);
                if (property == featureCode) {
                    parser = s -> singletonList(createResource(
                            GN_NS+record.get(CSV_HEADERS[6]) + "." + s));
                }

                if (property != null && parser != null)
                    parser.apply(value).forEach(node -> resource.addProperty(property, node));
            }
        }

        public Task setOnlyName(boolean onlyName) {
            this.onlyName = onlyName;
            return this;
        }
    }

    public static class Main {
        @Option(name = "--help", aliases = {"-h"}, help = true)
        private boolean help;

        @Option(name = "--in", required = true, usage = "Input CSV geonames dump")
        private File csv;

        @Option(name = "--only-name", usage = "Output only gn:name statements.")
        private boolean onlyName = false;

        @Option(name = "--tdb-out",
                usage = "Write output to the default graph of a TDB database")
        private File tdbOutput;

        @Option(name = "--rdf-out",
                usage = "Write output to a RDF file. Syntax is guessed from file extension")
        private File rdfOutput;

        public static void main(String[] args) throws Exception {
            Main main = new Main();
            CmdLineParser parser = new CmdLineParser(main);
            parser.parseArgument(args);
            if (main.tdbOutput == null && main.rdfOutput == null)
                throw new IllegalArgumentException("Both --tdb-out and --rdf-out missing");

            if (main.help)
                parser.printUsage(System.out);
            else
                main.run();
        }

        private void run() throws Exception {
            CSVParser parser;
            parser = CSVParser.parse(csv, CSV_CHARSET, CSV_FORMAT);
            Dataset dataset = null;
            Model m = ModelFactory.createDefaultModel();
            try {
                if (tdbOutput != null) {
                    dataset = TDBFactory.createDataset(tdbOutput.getPath());
                    m = dataset.getDefaultModel();
                    logger.info("Will output to TDB at {}", tdbOutput);
                }

                if (m.getNsURIPrefix(GN_NS) == null && m.getNsPrefixURI("gn") == null)
                    m.setNsPrefix("gn", GN_NS);
                if (m.getNsURIPrefix(WGS84_NS) == null && m.getNsPrefixURI("wgs84_pos") == null)
                    m.setNsPrefix("wgs84_pos", WGS84_NS);

                new Task(parser, m).setOnlyName(onlyName).call();

                if (rdfOutput != null) {
                    logger.info("Serializing {} triples into {}...", m.size(), rdfOutput);
                    Stopwatch watch = Stopwatch.createStarted();
                    try (FileOutputStream out = new FileOutputStream(rdfOutput)) {
                        Lang lang = RDFLanguages.filenameToLang(rdfOutput.getName());
                        RDFDataMgr.write(out, m, lang);
                    }
                    logger.info("Wrote {} MiB to {} in {}",
                            rdfOutput.length()/1048576.0, rdfOutput, watch);
                }

            } finally {
                if (!parser.isClosed()) parser.close();
                if (dataset != null) dataset.close();
                else m.close();
            }
        }
    }

}
