package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MaxTriplesStrategy implements AlignerInputStrategy {
    private static final Logger logger = LoggerFactory.getLogger(MaxTriplesStrategy.class);
    private static final long PROGRESS_INTERVAL = 10000;

    private int maxTriples;
    private Data data;
    private List<Resource> resources;
    private Iterator<Resource> resourcesIterator;


    public MaxTriplesStrategy(int maxTriples) {
        this.maxTriples = maxTriples;
    }

    protected MaxTriplesStrategy() {
        this.maxTriples = 0;
    }

    protected void setMaxTriples(int maxTriples) {
        Preconditions.checkState(this.data == null);
        this.maxTriples = maxTriples;
    }

    @Override
    public void init(@Nonnull Data data) {
        Preconditions.checkState(this.data == null);
        this.data = data;
        Model m = data.dataset.getDefaultModel();

        Stopwatch initWatch = Stopwatch.createStarted();
        Set<Property> properties = getSchemaProperties(data).stream()
                .map(r -> r.as(Property.class)).collect(Collectors.toSet());
        Map<Property, StmtIterator> its = new HashMap<>();
        properties.forEach(p -> its.put(p, m.listStatements(null, p, (RDFNode)null)));

        List<Property> bogusProperties = its.entrySet().stream()
                .filter(e -> !e.getValue().hasNext()).map(Map.Entry::getKey)
                .collect(Collectors.toList());
        properties.removeAll(bogusProperties);
        logger.info("Found {} useful and {} bogus properties on the schema in {}",
                properties.size(), bogusProperties.size(), initWatch);

        resources = new ArrayList<>(maxTriples);
        Stopwatch progressWatch = Stopwatch.createStarted();
        while (!its.isEmpty() && resources.size() < maxTriples) {
            initStep(its);
            if (progressWatch.elapsed(TimeUnit.MILLISECONDS) >= PROGRESS_INTERVAL) {
                progressWatch.reset().start();
                logger.info("Collecting subjects: {}/{} ({}%) {} elapsed", resources.size(),
                        maxTriples, Math.round((resources.size()/(double)maxTriples)*100),
                        initWatch);
            }
        }

        Stopwatch shuffleWatch = Stopwatch.createStarted();
        logger.info("Collected {} subjects in {}. Shuffling...", resources.size(), initWatch);
        Collections.shuffle(resources);
        logger.info("Shuffled subjects in {}", shuffleWatch);

        resourcesIterator = resources.iterator();
    }

    private void initStep(Map<Property, StmtIterator> its) {
        Comparator<Resource> comparator = Comparator.comparing(Resource::getURI);

        List<Property> exhausted = new ArrayList<>();
        for (Map.Entry<Property, StmtIterator> e : its.entrySet()) {
            if (!e.getValue().hasNext()) {
                e.getValue().close();
                exhausted.add(e.getKey());
            } else {
                Resource subject = e.getValue().next().getSubject();
                int idx = Collections.binarySearch(resources, subject, comparator);
                if (idx < 0) {
                    idx = -1 * (idx+1);
                    resources.add(idx, subject);
                }
            }
        }
        exhausted.forEach(its::remove);
    }

    private Set<Resource> getSchemaProperties(@Nonnull Data data) {
        Set<Resource> properties = new HashSet<>();
        Model as = data.schema;
        properties.addAll(as.listSubjectsWithProperty(RDF.type, RDF.Property).toList());
        properties.addAll(as.listSubjectsWithProperty(RDF.type, OWL2.ObjectProperty).toList());
        properties.addAll(as.listSubjectsWithProperty(RDF.type, OWL2.DatatypeProperty).toList());
        return properties;
    }

    @Nonnull
    @Override
    public Stream<Resource> nextStep() {
        return Stream.of(resourcesIterator.next());
    }

    @Override
    public boolean hasNextStep() {
        return data.output.size() < maxTriples && resourcesIterator.hasNext();
    }

    @Override
    public double getProgress() {
        return hasNextStep() ? data.output.size() / (double)maxTriples : 1;
    }

    @Override
    public void close() throws Exception { /* pass */ }
}
