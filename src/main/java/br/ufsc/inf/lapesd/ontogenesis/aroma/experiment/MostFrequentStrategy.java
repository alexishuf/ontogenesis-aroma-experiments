package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.mapdb.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.Math.ceil;
import static java.lang.Math.min;
import static org.apache.jena.rdf.model.ResourceFactory.createLangLiteral;

public class MostFrequentStrategy implements AlignerInputStrategy {
    private static final Logger log = LoggerFactory.getLogger(MostFrequentStrategy.class);

    private Data d;
    private final @Nonnull DB ontogenesisIndex;
    private final double frequencyThreshold;
    private SetMultimap<Property, RDFDatatype> observedDatatypes;
    private List<String> properties;
    private Iterator<String> propertyIterator;
    private Map<String, Map<String, Integer>> frequencyMap;
    private int totalPO, donePO;

    public MostFrequentStrategy(@Nonnull DB ontogenesisIndex, double frequencyThreshold) {
        this.ontogenesisIndex = ontogenesisIndex;
        this.frequencyThreshold = frequencyThreshold;
    }

    @Override
    public void init(@Nonnull Data data) {
        Preconditions.checkState(d == null);
        Preconditions.checkState(properties == null);
        this.d = data;
        //noinspection unchecked
        frequencyMap = (Map<String, Map<String, Integer>>)
                ontogenesisIndex.hashMap("map").open();

        totalPO = donePO = 0;
        observedDatatypes = HashMultimap.create();
        properties = new ArrayList<>();
        Model m = d.dataset.getDefaultModel();
        for (Map.Entry<String, Map<String, Integer>> entry : frequencyMap.entrySet()) {
            Property property = d.schema.createProperty(entry.getKey());
            if (!property.listProperties().hasNext()) continue;
            StmtIterator it = m.listStatements(null, property, (RDFNode) null);
            if (!it.hasNext()) {
                it.close();
                continue;
            }
            if (property.hasProperty(RDF.type, OWL2.ObjectProperty)) continue;

            properties.add(entry.getKey());
            int size = entry.getValue().size();
            totalPO += min(size, ceil(size * frequencyThreshold));

            while (it.hasNext()) {
                RDFNode o = it.next().getObject();
                if (o.isLiteral())
                    observedDatatypes.get(property).add(o.asLiteral().getDatatype());
            }
            it.close();
            log.info("Datatypes for {}: {}\n", property, observedDatatypes.get(property));
        }

        log.info("Found {} prop-object pairs for {} properties.\n", totalPO, properties.size());
        propertyIterator = properties.iterator();
    }

    @Nonnull
    @Override
    public Stream<Resource> nextStep() {
        String property = propertyIterator.next();
        Map<String, Integer> frequencies = frequencyMap.get(property);
        int limit = (int) ceil(frequencies.size() * frequencyThreshold);
        return frequencies.keySet().stream().limit(limit).parallel()
                .flatMap(o -> findResources(property, o).stream());
    }

    private List<Resource> findResources(String propertyURI, String objectLexicalForm) {
        Property property = ResourceFactory.createProperty(propertyURI);
        Model data = this.d.dataset.getDefaultModel();
        Set<Literal> literals = observedDatatypes.get(property).stream()
                .map(dt -> ResourceFactory.createTypedLiteral(objectLexicalForm, dt))
                .collect(Collectors.toSet());
        literals.add(ResourceFactory.createPlainLiteral(objectLexicalForm));
        d.langCodes.stream().map(lang -> createLangLiteral(objectLexicalForm, lang))
                .forEach(literals::add);

        List<Resource> list = literals.stream()
                .flatMap(l -> data.listSubjectsWithProperty(property, l).toList().stream())
                .collect(Collectors.toList());
        synchronized (this) {
            ++donePO;
        }
        return list;
    }

    @Override
    public boolean hasNextStep() {
        return propertyIterator.hasNext();
    }

    @Override
    public synchronized double getProgress() {
        return donePO/(double)totalPO;
    }

    @Override
    public void close() throws Exception {
        ontogenesisIndex.close();
    }
}
