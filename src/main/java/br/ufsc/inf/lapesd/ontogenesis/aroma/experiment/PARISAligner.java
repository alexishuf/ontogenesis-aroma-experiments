package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paris.Paris;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PARISAligner extends Aligner {
    private static final Logger logger = LoggerFactory.getLogger(PARISAligner.class);

    public static void main(String[] args) throws Exception {
        new PARISAligner().parseAndRun(args);
    }

    @Override
    public Model align() throws Exception {
        File outdir = new File(out.getParentFile(), out.getName() + "-paris");
        if (outdir.exists() && outdir.isDirectory())
            FileUtils.deleteDirectory(outdir);
        if (outdir.exists() && !outdir.isDirectory() && !outdir.delete())
            throw new IOException("Could not delete" + outdir);
        if (!new File(outdir, "results").mkdirs())
            throw new IOException("Could not create results in " + outdir);
        File store1 = new File(outdir, "store1");
        if (!store1.mkdirs())
            throw new IOException("Could not create store1 in " + outdir);
        File store2 = new File(outdir, "store2");
        if (!store2.mkdirs())
            throw new IOException("Could not create store2 in " + outdir);

        makeNTriples(left, store1);
        makeNTriples(right, store2);

        String settings = String.format("resultTSV = %1$s/results\n" +
                "factstore1 = %1$s/store1\n" +
                "factstore2 = %1$s/store2\n" +
                "home = %1$s\n", outdir);
        File settingsFile = Files.createTempFile("settings", "").toFile();
        settingsFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(settingsFile)) {
            IOUtils.write(settings, out, StandardCharsets.UTF_8);
        }
        logger.info("PARIS settings at {}: {}", settingsFile, settings);

        Stopwatch watch = Stopwatch.createStarted();
        try {
            Paris.main(new String[] {settingsFile.getPath()});
        } finally {
            FileUtils.deleteQuietly(settingsFile);
        }
        logger.info("PARIS took {}", watch);

        return ModelFactory.createDefaultModel();
    }

    private File makeNTriples(@Nonnull File file, @Nonnull File outDir) throws IOException {
        Preconditions.checkArgument(!outDir.equals(file.getParentFile()));
        Matcher matcher = Pattern.compile("(.*)(\\.[^.]+)$").matcher(file.getName());
        if (!matcher.matches())
            throw new IllegalArgumentException("Bad filename: " + file.getName());

        File ntFile;
        ntFile = new File(outDir, matcher.group(1) + "." + Lang.NT.getFileExtensions().get(0));
        if (ntFile.exists() && !ntFile.delete())
            throw new IOException("Could not delete " + ntFile);

        Lang lang = RDFLanguages.filenameToLang(file.getName());
        if (lang.equals(Lang.NT)) {
            FileUtils.copyFile(file, ntFile);
            logger.info("Copied {} to {}", file, ntFile);
            return ntFile;
        }

        Model model = ModelFactory.createDefaultModel();
        Stopwatch watch = Stopwatch.createStarted();
        try (FileInputStream in = new FileInputStream(file);
             FileOutputStream out = new FileOutputStream(ntFile)) {
            RDFDataMgr.read(model, in, lang);
            RDFDataMgr.write(out, model, Lang.NT);
        } finally {
            model.close();
        }
        logger.info("Converted {} to {} in {}.", file, ntFile, watch);
        return ntFile;
    }
}
