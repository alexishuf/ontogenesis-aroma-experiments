package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import java.util.List;

public class PoliceReport {
    public String idBO;
    public String numero;
    public String idDelegacia;
    public String tipoBoletim;
    public String dependencia;
    public String local;
    public String tipoLocal;
    public String circunscricao;
    public String dataOcorrencia;
    public String turnoOcorrencia;
    public String dataComunicacao;
    public String horaComunicacao;
    public String dataElaboracao;
    public String horaElaboracao;
    public String flagrante;
    public String examesRequisitados;
    public String solucao;
    public String categoria;

    public List<Parte> partesEnvolvidas;
    public List<Natureza> naturezas;

    public static class Parte {
        public String nome;
        public String tipoEnvolvimento;
        public String rg;
        public String naturalidade;
        public String nacionalidade;
        public String sexo;
        public String dataNascimento;
        public String idade;
        public String estadoCivil;
        public String instrucao;
        public String profissao;
        public String cutis;
        public String naturezasEnvolvidas;
    }

    public static class Natureza {
        public String especie;
        public String descricao;
    }
}
