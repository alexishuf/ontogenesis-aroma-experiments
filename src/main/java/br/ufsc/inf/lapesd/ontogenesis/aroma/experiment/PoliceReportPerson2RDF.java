package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.gson.Gson;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.jetbrains.annotations.NotNull;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static org.apache.jena.rdf.model.ResourceFactory.createProperty;
import static org.apache.jena.rdf.model.ResourceFactory.createResource;

public class PoliceReportPerson2RDF {
    public static class Task {
        private @Nonnull Iterator<Model> personIterator;
        private @Nonnull File outputFile;
        private @Nonnull Lang outputLang;
        private Model cacheModel;
        private int count;

        public Task(@Nonnull Iterator<Model> personIterator, @Nonnull File outputFile,
                    @Nonnull Lang outputLang, @Nonnull Model ontology) throws IOException {
            this.personIterator = personIterator;
            this.outputFile = outputFile;
            this.outputLang = outputLang;
            cacheModel = ModelFactory.createDefaultModel();
            cacheModel.add(ontology);
            write();
            this.count = 0;
        }

        public void addPersonUntil(int personCount) throws IOException {
            for (; (personCount < 0 || count < personCount) && personIterator.hasNext(); count++) {
                cacheModel.add(personIterator.next());
            }
            write();
        }

        public void addNextPerson() throws IOException {
            cacheModel.add(personIterator.next());
            write();
        }

        public boolean hasNextPerson() {
            return personIterator.hasNext();
        }

        private void write() throws IOException {
            try (FileOutputStream out = new FileOutputStream(outputFile)) {
                RDFDataMgr.write(out, cacheModel, outputLang);
            }
        }
    }

    public static class Repository implements Iterable<Model> {
        private static final Logger logger = LoggerFactory.getLogger(Repository.class);
        private static final String NS = "http://127.0.0.1:8081/criminal-report-person-microservice/ontology#";
        private List<File> shuffled;

        public Repository(@Nonnull File dir) {
            Preconditions.checkArgument(dir.exists());
            Preconditions.checkArgument(dir.isDirectory());

            Stopwatch watch = Stopwatch.createStarted();
            File[] files = dir.listFiles(file ->
                    Pattern.matches("\\d{4}-\\d+-\\d+.json", file.getName()));
            Preconditions.checkArgument(files != null, "No report json files found!");

            shuffled = new ArrayList<>(files.length);
            Collections.addAll(shuffled, files);
            Collections.shuffle(shuffled);
            logger.info("Shuffled {} police report jsons in {}", shuffled.size(), watch);
        }

        public static Stream<Model> extractPersons(File jsonFile)  {
            try (FileReader reader = new FileReader(jsonFile)) {
                PoliceReport report = new Gson().fromJson(reader, PoliceReport.class);
                return report.partesEnvolvidas.stream().map(p -> {
                    Model m = ModelFactory.createDefaultModel();
                    Resource r = m.createResource(createResource(NS+"Class1"));
                    if (p.nome != null) {
                        String primeiroNome = p.nome.split(" +")[0];
                        r.addProperty(createProperty(NS + "primeiroNome"), primeiroNome)
                                .addProperty(createProperty(NS+"nome"), p.nome);
                    }
                    if (p.rg != null)
                        r.addProperty(createProperty(NS+"rg"), p.rg);
                    if (p.naturalidade != null)
                        r.addProperty(createProperty(NS+"naturalidade"), p.naturalidade);
                    if (p.nacionalidade != null)
                        r.addProperty(createProperty(NS+"nacionalidade"), p.nacionalidade);
                    if (p.profissao != null)
                        r.addProperty(createProperty(NS+"profissao"), p.profissao);
                    if (p.instrucao != null)
                        r.addProperty(createProperty(NS+"instrucao"), p.instrucao);
                    if (p.dataNascimento != null)
                        r.addProperty(createProperty(NS+"dataNascimento"), p.dataNascimento);
                    if (p.sexo != null)
                        r.addProperty(createProperty(NS+"sexo"), p.sexo);
                    if (p.cutis != null)
                        r.addProperty(createProperty(NS+"cutis"), p.cutis);

                    return m;
                });

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @NotNull
        @Override
        public Iterator<Model> iterator() {
            return shuffled.stream().flatMap(Repository::extractPersons).iterator();
        }
    }

    public static class Main {
        private static final Logger logger = LoggerFactory.getLogger(Main.class);

        @Option(name = "--help", aliases = {"-h"}, help = true)
        private boolean help;

        @Option(name = "--repository", required = true,
                usage = "Directory with json files of police reports. JSON files with the format " +
                        "{year}-idDelegacia}-{numero} will be considered for conversion")
        private File repository;

        @Option(name = "--ontology", required = true,
                usage = "Ontology to be added, without modification to --out")
        private File ontology;

        @Option(name = "--count",
                usage = "Number of reports to convert and add to the output")
        private int count = -1;

        @Option(name = "--out", required = true, usage = "Alignment ontology output")
        private File output;

        public static void main(String[] args) throws Exception {
            Main main = new Main();
            CmdLineParser parser = new CmdLineParser(main);
            parser.parseArgument(args);
            if (main.help)
                parser.printUsage(System.out);
            else
                main.run();
        }

        private void run() throws IOException {
            Repository repository = new Repository(this.repository);
            Model ontologyModel = ModelFactory.createDefaultModel();

            try (FileInputStream in = new FileInputStream(ontology)) {
                RDFDataMgr.read(ontologyModel, in, guessLanguage(ontology.getName()));
            }

            Lang lang = guessLanguage(output.getName());
            Stopwatch watch = Stopwatch.createStarted();
            Task task = new Task(repository.iterator(), output, lang, ontologyModel);
            task.addPersonUntil(count);
            logger.info("Wrote {} person instances to {} in {}", task.count, output, watch);
        }

        @Nonnull
        private Lang guessLanguage(String name) {
            Lang lang = RDFLanguages.filenameToLang(name);
            Preconditions.checkArgument(lang != null, "Could not guess RDF language from " + name);
            return lang;
        }
    }
}
