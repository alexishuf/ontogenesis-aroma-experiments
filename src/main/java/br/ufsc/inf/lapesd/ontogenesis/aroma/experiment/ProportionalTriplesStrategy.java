package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import org.mapdb.DB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.Map;

public class ProportionalTriplesStrategy extends MaxTriplesStrategy {
    private final static Logger logger = LoggerFactory.getLogger(ProportionalTriplesStrategy.class);
    private final @Nonnull DB ontogenesisIndex;
    private final double proportionalFrequency;

    public ProportionalTriplesStrategy(@Nonnull DB ontogenesisIndex, double proportionalFrequency) {
        this.ontogenesisIndex = ontogenesisIndex;
        this.proportionalFrequency = proportionalFrequency;
    }

    @Override
    public void init(@Nonnull Data data) {
        //noinspection unchecked
        Map<String, Map<String, Integer>> map = (Map<String, Map<String, Integer>>)
                ontogenesisIndex.hashMap("map").open();

        long size = map.keySet().stream()
                .filter(u -> data.schema.createResource(u).listProperties().hasNext())
                .map(u -> Math.round(map.get(u).size()*proportionalFrequency))
                .reduce(0L, Long::sum);
        if (size > Integer.MAX_VALUE)
            throw new IllegalArgumentException("Computed maxTriples=" + size + " is too large");
        logger.info("Determined maxTriples={} for {}% of data in ontogenesis index",
                size, proportionalFrequency*100);

        super.setMaxTriples((int)size);
        super.init(data);
    }

    @Override
    public void close() throws Exception {
        super.close();
        ontogenesisIndex.close();
    }
}
