package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import com.google.common.base.Stopwatch;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.tdb.TDBFactory;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class SharedValuesStats {
    private static Logger logger = LoggerFactory.getLogger(SubjectJoinStats.class);
    private static final int PROGRESS_INTERVAL = 10000;

    private static final String NS = "http://127.0.0.1:8081/criminal-report-person-microservice/ontology#";
    private static final Set<Property> targetProperties;

    static {
        targetProperties = new HashSet<>();
        targetProperties.add(ResourceFactory.createProperty(NS+"primeiroNome"));
        targetProperties.add(ResourceFactory.createProperty(NS+"cutis"));
        targetProperties.add(ResourceFactory.createProperty(NS+"sexo"));
        targetProperties.add(ResourceFactory.createProperty(NS+"nome"));
        targetProperties.add(ResourceFactory.createProperty(NS+"nacionalidade"));
        targetProperties.add(ResourceFactory.createProperty(NS+"naturalidade"));
        targetProperties.add(ResourceFactory.createProperty(NS+"profissao"));
        targetProperties.add(ResourceFactory.createProperty(NS+"instrucao"));
    }

    @Option(name = "--help", aliases = {"-h"}, help = true)
    private boolean help;

    @Option(name = "--left", required = true, usage = "Left hand TDB directory with person data")
    private File leftTDBDir;

    @Option(name = "--right", required = true, usage = "Right hand TDB directory")
    private File rightTDBDir;

    @Option(name = "--out", required = true, usage = "Output CSV file")
    private File outCSV;

    private Stopwatch lastProgress;

    public static void main(String[] args) throws Exception {
        SharedValuesStats app = new SharedValuesStats();
        CmdLineParser parser = new CmdLineParser(app);
        parser.parseArgument(args);
        if (app.help)
            parser.printUsage(System.out);
        else
            app.run();
    }

    private void run() throws Exception {
        Dataset leftDataset = null, rightDataset = null;
        try {
            leftDataset = TDBFactory.createDataset(leftTDBDir.getPath());
            rightDataset = TDBFactory.createDataset(rightTDBDir.getPath());
            Model left = leftDataset.getDefaultModel();
            Model right = rightDataset.getDefaultModel();


            Stopwatch totalWatch = Stopwatch.createStarted();
            long occurrences = countOccurrences(left, right);
            logger.info("Found {} occurrences of left values in right in {}. " +
                    "targetProperties={}", occurrences, totalWatch, targetProperties);

            try (FileWriter writer = new FileWriter(outCSV);
                 CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT)) {
                printer.printRecord("count");
                printer.printRecord(occurrences);
            }

        } finally {
            if (leftDataset != null) leftDataset.close();
            if (rightDataset != null) rightDataset.close();
        }
    }

    private long countOccurrences(Model left, Model right) {
        lastProgress = Stopwatch.createStarted();
        SubjectJoinStats.LevenshteinSelector ls;
        ls = new SubjectJoinStats.LevenshteinSelector(right, 1);
        long occurrences = 0, done = -1, total = left.size();
        HashSet<String> visited = new HashSet<>();
        StmtIterator it = left.listStatements(null, null, (RDFNode) null);
        while (it.hasNext()) {
            ++done;
            if (lastProgress.elapsed(TimeUnit.MILLISECONDS) >= PROGRESS_INTERVAL) {
                lastProgress.reset().start();
                logger.info("countOccurrences processed {}/{} ({}%) triples.",
                        done, total, Math.round(done/(double)total*100));
            }

            Statement s = it.next();
            if (!s.getObject().isLiteral() || !targetProperties.contains(s.getPredicate()))
                continue;
            Literal literal = s.getObject().asLiteral();
            String lex = literal.getLexicalForm();
            if (visited.contains(lex))
                continue;
            visited.add(lex);

            Set<Resource> set = ls.apply(Collections.singleton(literal));
            if (!set.isEmpty())
                logger.info("Subjects with {}: {}", lex, set);
            occurrences += set.size();
        }

        return occurrences;
    }
}
