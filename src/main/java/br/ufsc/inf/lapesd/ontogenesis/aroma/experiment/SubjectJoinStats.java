package br.ufsc.inf.lapesd.ontogenesis.aroma.experiment;

import com.google.common.base.Stopwatch;
import com.google.common.cache.*;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.tdb.TDBFactory;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SubjectJoinStats {
    private static Logger logger = LoggerFactory.getLogger(SubjectJoinStats.class);
    private static int PROGRESS_INTERVAL = 10000;

    @Option(name = "--help", aliases = {"-h"})
    private boolean help;

    @Option(name = "--left", required = true, usage = "TDB directory with instance data")
    private File leftTDBDir;

    @Option(name = "--right", required = true, usage = "TDB directory with instance data")
    private File rightTDBDir;

    @Option(name = "--out", required = true, usage = "Path to output CSV file")
    private File outCsv;

    private Stopwatch lastProgress;

    public static void main(String[] args) throws Exception {
        SubjectJoinStats app = new SubjectJoinStats();
        CmdLineParser parser = new CmdLineParser(app);
        parser.parseArgument(args);
        if (app.help)
            parser.printUsage(System.out);
        else
            app.run();
    }

    private void run() throws Exception {
        Dataset leftDataset = null, rightDataset = null;
        try {
            leftDataset = TDBFactory.createDataset(leftTDBDir.getPath());
            rightDataset = TDBFactory.createDataset(rightTDBDir.getPath());
            Model left = leftDataset.getDefaultModel();
            Model right = rightDataset.getDefaultModel();
            logger.info("Left TDB has {} triples and right has {} triples and {} subjects",
                    left.size(), right.size(), countSubjects(right));

            Set<Property> properties = getDatatypeProperties(left);
            logger.info("{} owl:DatatypeProperties: {}", properties.size(), properties);
            Set<Resource> leftResources = findLeftResources(left, properties);

            logger.info("Counting correspondents under equality...");
            lastProgress = Stopwatch.createStarted();
            Stopwatch watch = Stopwatch.createStarted();
            SortedMap<Integer, Integer> eqMap = countCorrespondent(leftResources, properties,
                    os -> equalitySelect(os, right));
            logger.info("Counted correspondents under equality in {}", watch);

            SortedMap<Integer, Integer> ld1Map;
            ld1Map = countCorespondentWithLevenshtein(properties, leftResources, right, 1);

            ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
            try (FileOutputStream fileOut = new FileOutputStream(outCsv);
                 TeeOutputStream teeOut = new TeeOutputStream(fileOut, byteOut);
                 OutputStreamWriter writer = new OutputStreamWriter(teeOut);
                 CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT)) {
                printer.printRecord("propertyCount", "rightSubjectCountEquality",
                        "rightSubjectCountLevenshtein1");
                for (int i = 1; i < properties.size(); i++)
                    printer.printRecord(i, eqMap.get(i), ld1Map.get(i));
            }
            logger.info("Wrote results to {}:\n{}", outCsv,
                    byteOut.toString(StandardCharsets.UTF_8.name()));

        } finally {
            if (leftDataset != null) leftDataset.close();
            if (rightDataset != null) rightDataset.close();
        }
    }

    private SortedMap<Integer, Integer> countCorespondentWithLevenshtein(Set<Property> properties, Set<Resource> leftResources, Model right, int threshold) {
        logger.info("Counting correspondents under Levenshtein distance < {}...", threshold);
        lastProgress = Stopwatch.createStarted();
        Stopwatch watch = Stopwatch.createStarted();
        LevenshteinSelector ls = new LevenshteinSelector(right, threshold);
        SortedMap<Integer, Integer> map = countCorrespondent(leftResources, properties, ls);
        ls.close();
        logger.info("Counted correspondents under Levenshtein < {} in {}", threshold, watch);
        return map;
    }

    private long countSubjects(Model right) {
        Stopwatch lastProgress = Stopwatch.createStarted();
        Set<Resource> resources = new HashSet<>();
        for (StmtIterator it = right.listStatements(); it.hasNext(); ) {
            if (lastProgress.elapsed(TimeUnit.MILLISECONDS) >= PROGRESS_INTERVAL) {
                logger.info("Counting subjects: {}", resources.size());
                lastProgress.reset().start();
            }
            resources.add(it.next().getSubject());
        }
        return resources.size();
    }

    private Set<Resource> findLeftResources(Model left, Set<Property> properties) {
        return properties.stream()
                .flatMap(p -> left.listResourcesWithProperty(p).toSet().stream())
                .collect(Collectors.toSet());
    }

    private SortedMap<Integer, Integer>
    countCorrespondent(Set<Resource> leftResources, Set<Property> properties,
                       Function<Set<Literal>, Set<Resource>> selector) {
        SetMultimap<Integer, Resource> resources = HashMultimap.create();
        int count = 0;
        for (Resource l : leftResources) {
            ++count;
            if (lastProgress.elapsed(TimeUnit.MILLISECONDS) >= PROGRESS_INTERVAL) {
                logger.info("Processed {}/{} resources", count, leftResources.size());
                lastProgress.reset().start();
            }
            SetMultimap<Integer, Resource> mmap = findCorrespondent(l, properties, selector);
            mmap.keys().forEach(k -> resources.get(k).addAll(mmap.get(k)));

        }
        TreeMap<Integer, Integer> result = new TreeMap<>();
        resources.keys().forEach(k -> result.put(k, resources.get(k).size()));
        return result;
    }

    private SetMultimap<Integer, Resource>
    findCorrespondent(Resource resource, Set<Property> properties,
                      Function<Set<Literal>, Set<Resource>> selector) {
        Model left = resource.getModel();
        List<Set<Resource>> subjects = properties.stream()
                .map(p -> selector.apply(left.listObjectsOfProperty(resource, p).toSet().stream()
                        .filter(RDFNode::isLiteral).map(RDFNode::asLiteral)
                        .collect(Collectors.toSet())))
                .collect(Collectors.toList());
        assert subjects.size() == properties.size();
        subjects.sort(Comparator.comparing(Set::size));

        SetMultimap<Integer, Resource> resources = HashMultimap.create();

        HashSet<Integer> indexes = IntStream.range(0, subjects.size())
                .collect(HashSet::new, HashSet::add, HashSet::addAll);
        Sets.powerSet(indexes).stream().parallel().filter(s -> !s.isEmpty()).forEach(subset -> {
            HashSet<Resource> join = new HashSet<>();
            List<Integer> idxList = subset.stream().sorted().collect(Collectors.toList());
            join.addAll(subjects.get(idxList.get(0)));
            idxList.stream().skip(1).forEach(i -> join.retainAll(subjects.get(i)));
            synchronized (resources) {
                resources.get(subset.size()).addAll(join);
            }
        });

        return resources;
    }

    private Set<Resource> equalitySelect(Set<Literal> values, Model right) {
        List<StmtIterator> list = values.stream()
                .map(v -> right.listStatements(null, null, v)).collect(Collectors.toList());
        HashSet<Resource> selected = new HashSet<>();
        for (Iterator<StmtIterator> listIt = list.iterator(); listIt.hasNext(); ) {
            StmtIterator it = listIt.next();
            if (!it.hasNext())
                listIt.remove();
            else
                selected.add(it.next().getSubject());
        }
        return selected;
    }

    static class LevenshteinSelector implements Function<Set<Literal>, Set<Resource>> {
        private final Model model;
        private LoadingCache<String, Set<Resource>> cache;
        private final LevenshteinDistance ld;


        LevenshteinSelector(Model model, int threshold) {
            this.model = model;
            ld = new LevenshteinDistance(threshold);
            cache = CacheBuilder.<String, Set<Resource>>newBuilder()
                    .concurrencyLevel(Runtime.getRuntime().availableProcessors())
                    .softValues()
                    .build(new CacheLoader<String, Set<Resource>>() {
                        @Override
                        public Set<Resource> load(@Nonnull String key) throws Exception {
                            return findResources(key);
                        }
                    });
        }

        private Set<Resource> findResources(String lexicalForm) {
            HashSet<Resource> set = new HashSet<>();
            Stopwatch progressWatch = Stopwatch.createStarted();
            Stopwatch startWatch = Stopwatch.createStarted();
            StmtIterator it = model.listStatements(null, null, (RDFNode) null);
            long total = model.size();
            long count = -1;
            boolean showDone = false;
            while (it.hasNext()) {
                ++count;
                if (progressWatch.elapsed(TimeUnit.MILLISECONDS) >= PROGRESS_INTERVAL) {
                    progressWatch.reset().start();
                    logger.info("findResources({}): {}/{} ({}%) triples",
                            lexicalForm, count, total, Math.round(count/(double)total*100));
                    showDone = true;
                }
                Statement s = it.next();
                if (!s.getObject().isLiteral()) continue;
                if (ld.apply(lexicalForm, s.getObject().asLiteral().getLexicalForm()) >= 0)
                    set.add(s.getSubject());
            }
            if (showDone)
                logger.info("findResources({}): done in {}", lexicalForm, startWatch);
            return set;
        }

        @Override
        public Set<Resource> apply(Set<Literal> literals) {
            Set<Resource> result = new HashSet<>();
            for (Literal literal : literals) {
                String lex = literal.getLexicalForm();
                Set<Resource> resources;
                try {
                    resources = cache.get(lex);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
                result.addAll(resources);
            }

            return result;
        }

        public void close() {
            cache.invalidateAll();
        }
    }

    private Set<Property> getDatatypeProperties(Model left) {
        HashSet<Property> properties = new HashSet<>();
        StmtIterator it = left.listStatements(null, null, (RDFNode) null);
        while (it.hasNext()) {
            Statement s = it.next();
            if (s.getObject().isLiteral())
                properties.add(s.getPredicate());
        }
        it.close();
        return properties;
    }
}
